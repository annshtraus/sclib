﻿app.service('tracksService', function($http, $q) {
	this.data = null;
    this.searchString = '';
    this.history = [];
    this.offset = 6;
    this.listSize = 6;
    this.historyListSize = 5;
    this.clientID = '​652006c469530a4a7d6184b18e16c81';
    
    this.getTracks = function(options) {
        var deferred = $q.defer();
        
        this.searchString = options.searchString;
		
		var params = {
			q: this.searchString,
			limit: this.listSize,
			linked_partitioning: 1
		}
		
		if (options.offset) {
			params.offset = this.offset;
			this.offset += this.listSize;
		} else {
			this.offset = 6;
		}
		
		var self = this;
		
		SC.get('/tracks', params, function(tracks, error) {
			if (error) {
				deferred.reject("An error occured while fetching items");
			}
			deferred.resolve(tracks);
			self.data = tracks;
			self.data.search_string = self.searchString;
			self.data.output_string = self.data.search_string;
			if (self.offset > self.listSize) {
				self.data.page = self.offset / self.listSize;
				self.data.output_string += ' [page ' + self.data.page + ']';
			}
			self.saveHistory();
		});
		
		return deferred.promise;
    }
    
    this.saveHistory = function() {
    	this.history.push(this.data);
    	if (this.history.length > this.historyListSize) this.history.shift();
    	localStorage.setItem("history", JSON.stringify(this.history));
    }
    
    this.reviveHistory = function() {
    	this.history = JSON.parse(localStorage.getItem("history")) || [];
    	return this.history;
    }
    
    this.getPlayerHTML = function(trackURL) {
    	var deferred = $q.defer();
    	
    	SC.oEmbed(trackURL, { auto_play: true }, function(oEmbed, error) {
			if (!oEmbed || !oEmbed.html || error) {
				deferred.reject("An error occured while fetching the track");
    			return;
			}
			
			deferred.resolve(oEmbed.html);
		});
		
		return deferred.promise;
    }

});