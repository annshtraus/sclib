﻿app.controller('TracksController', function ($scope, $sce, tracksService) {
	var clientID = '​652006c469530a4a7d6184b18e16c81',
		noImageURL = 'img/icon-no-image.png',
		viewStyle = 'list';
    
    init();

    function init() {
    	$scope.imgURL = 'img/cover-image.png';
		$scope.noImageURL = noImageURL;
		$scope.viewStyle = localStorage.getItem('viewStyle') || viewStyle;
		$scope.trackURL = '';
		$scope.playerHTML = '';
		$scope.searchError = false;
		
		$scope.history = tracksService.reviveHistory();
		
		SC.initialize({
			client_id: clientID
		});
    }
    
    $scope.changeViewStyle = function(style) {
    	$scope.viewStyle = style;
    	localStorage.setItem('viewStyle', style);
    }
    
    $scope.getTracks = function(offset) {
    	if (!$scope.searchText) {
            $scope.searchError = true;
            return;
        } else {
            $scope.searchError = false;
        }
    	
    	var options = {
    		searchString: $scope.searchText
    	}
    	
    	if (offset) options.offset = offset;
    	
    	tracksService.getTracks(options).then(function(data) {
			$scope.tracks = data.collection;
		},
		function(errorMessage) {
			$scope.error = errorMessage;
		});
    }
    
    $scope.showImage = function(track) {
    	$scope.imgURL = track.artwork_url || noImageURL;
    	$scope.trackURL = track.uri;
    }
    
    $scope.showTracksFromHistory = function(tracks) {
    	$scope.tracks = tracks.collection;
    }
    
    $scope.playTrack = function() {
    	tracksService.getPlayerHTML($scope.trackURL).then(function(data) {
    		$scope.playerHTML = $sce.trustAsHtml(data);
		},
		function(errorMessage) {
			$scope.error = errorMessage;
		});
    }
    
});