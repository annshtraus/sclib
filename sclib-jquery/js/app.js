(function($, window, document) {

var Library = new function() {
    this.data = null;
    this.searchString = '';
    this.history = [];
    this.offset = 6;
    this.clientID = '​652006c469530a4a7d6184b18e16c81';
    this.listSize = 6;
    this.historyListSize = 5;
    this.viewStyle = null;
    this.noImageURL = 'img/icon-no-image.png';
    
    this.getData = function(offset) {
        this.searchString = $('#searchText').val();

        if (/^[\s+]*$/.test(this.searchString)) { // if input value contains only whitespaces
            $('#searchError').show();
            return;
        } else {
            if ($('#searchError').is(':visible')) {
                $('#searchError').hide();
            }
        }

        $('#loadingContainer').fadeIn();

		var self = this;
		
		var params = {
			q: this.searchString,
			limit: this.listSize,
			linked_partitioning: 1
		}
		
		if (offset) {
			params.offset = this.offset;
			this.offset += this.listSize;
		} else {
			this.offset = 6;
		}
		
		SC.get('/tracks', params, function(tracks, error) {
			if (error) {
				self.alertError(error);
			}
			self.data = tracks;
			self.data.search_string = self.searchString;
			if (self.offset > self.listSize) {
				self.data.page = self.offset / self.listSize;
			}
			self.saveHistory();
			self.displayData();
		});
    };

    this.alertError = function(errorMessage) {
        alert('Server responded:\n' + errorMessage + '.\n\nPlease try again later.');
        $('#loadingContainer').fadeOut();
    }

    this.displayData = function() {
        this.displaySearchResults();
        this.displayRecentSearches();

        $('#loadingContainer').fadeOut();
    }
    
    this.displaySearchResults = function(historyId) {
    	if (typeof(historyId) != 'undefined') {
    		this.data = this.history[historyId];
    	}
    	
    	if (!this.data) return; // we're just changing the view style
    	
    	var data = this.data.collection;
    	if ($.isEmptyObject(data)) {
    		this.alertError('Search result is empty, please try again');
    		return;
    	}
    	
    	var searchDiv = $('#searchResults');
    	searchDiv.empty();
    	searchDiv.toggleClass('row', (this.viewStyle == 'tile'));
    	
    	var onclick = '',
    		imgURL = '',
    		searchList = [];
    	
    	for (var i in data) {
    		if (this.viewStyle == 'tile') {
    			imgURL = data[i].artwork_url || this.noImageURL;
    			searchList.push('<div class="col-md-6"><a class="thumbnail" data-result-id="' + i + '"><img src="' + imgURL + '"></a></div>');
    		} else {
    			searchList.push('<a class="list-group-item" data-result-id="' + i + '">' + data[i].title + '</a>');
    		}
    	}
    	
    	searchDiv.append(searchList.join(''));
    }
    
    this.displayRecentSearches = function() {
    	$('#recentSearches').empty();
    	this.showHistory();
    }
    
    this.showImage = function(id) {
    	var img = $('#currentImage'),
    		self = this;
		
    	img.fadeOut(400, function() {
            img.attr('src', self.data.collection[id].artwork_url || self.noImageURL);
            img.data('track-id', id);
        })
        .fadeIn(400);
    }
    
    this.playTrack = function(id) {
    	var self = this;
    	
    	SC.oEmbed(this.data.collection[id].uri, { auto_play: true }, function(oEmbed) {
			if (!oEmbed || !oEmbed.html) {
				self.alertError('Error loading track, please try again');
    			return;
			}
			
			$('#imageContainer').find('iframe').remove();
			$('#imageContainer').append(oEmbed.html);
		});
	}

	this.checkStorage = function() {
		if (typeof(Storage) === "undefined") {
    		this.alertError('Unable to use local storage');
    		return false;
    	}
    	
    	return true;
	}

    this.saveHistory = function() {
    	this.history.push(this.data);
    	if (this.history.length > this.historyListSize) this.history.shift();
    	
    	if (!this.checkStorage) {
    		return;
    	}
    	localStorage.setItem("history", JSON.stringify(this.history));
    }
    
    this.reviveHistory = function() {
    	if (!this.checkStorage) {
    		return;
    	}
    	
    	this.history = JSON.parse(localStorage.getItem("history")) || [];
    	this.showHistory();
    }
    
    this.showHistory = function() {
    	var historyList = [],
    		text = '';
    	for (var i = 0; i < this.history.length; i++) {
			text = this.history[i].search_string;
			if (this.history[i].page) text += ' [page ' + this.history[i].page + ']';
			historyList.push('<a class="list-group-item" data-history-id="' + i + '">' + text + '</a>');
    	}
    	
    	$('#recentSearches').append(historyList.join(''));
    }

    this.init = function() {
        this.initSC();
        this.initViewStyle();
        this.setEventHandlers();
        this.reviveHistory();
    }
    
    this.initSC = function() {
    	SC.initialize({
			client_id: this.clientID
		});
    }
    
    this.initViewStyle = function() {
    	this.viewStyle = localStorage.getItem('viewStyle');
    }
    
    this.changeViewStyle = function(style) {
    	this.viewStyle = style;
    	localStorage.setItem('viewStyle', style);
    	this.displaySearchResults();
    }
    
    this.setEventHandlers = function() {
    	var self = this;
    	
    	$('#recentSearches').on('click', 'a', function(event) {
    		var id = $(event.target).data('history-id');
    		self.displaySearchResults(id);
    	});
    	
    	$('#searchResults').on('click', 'a', function(event) {
    		var id = $(this).data('result-id');
    		self.showImage(id);
    	});
    	
    	$('#navButtons').on('click', 'button', function(event) {
    		var btn = $(this).data('btn-name');
    		switch(btn) {
    			case 'btnNext':
    				self.getData(true);
    				break;
    			case 'btnList':
    				self.changeViewStyle('list');
    				break;
    			case 'btnTile':
    				self.changeViewStyle('tile');
    				break;
    		}
    	});
    	
    	$('#currentImage').on('click', function(event) {
    		var id = $(event.target).data('track-id');
    		if (typeof(id) != 'undefined') self.playTrack(id);
    	});
    	
    	$('#btnGo').on('click', function() {
    		self.getData();
    	});
    }
}

$(document).ready(function() {
    Library.init();
});

}(window.jQuery, window, document));